# Status-code
Status-code can be used to check the status of your web properties by checking URLS from a file. Status-Code can also be used as a bug bounty information gathering tool this can grab information from wayback machine. 
It can also hunt for sub-domains of a target via sublist3r and bruteforce via subbrute. Status code will create a html report including screenshots of any thing that is live. This program
is for use on Kali linux or another distro of Linux.

## Dependancys Needed 
python3, python3-requests, python3-selenium 

## Usage

### To check web properties from a file containing urls
All urls need to be on a new line.

./status-code -f "Filename here" -o "Name of a file to contain the results. don't add an extension"


### To Grab Information from wayback machine 

./status-code -w "www.domain.com" -o "Name of a file to contain the results. don't add an extension"


### To use sublist3r or the brute force module 

./status-code -d "domain.com" -o "Name of a file to contain the results. don't add an extension"/
./status-code -d "domain.com" -s y -o "Name of a file to contain the results. don't add an extension"


## Reporting 

The reports will be contained in the reports folder they will be in html format. To view right click on a report and select open with a browser. 
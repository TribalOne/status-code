#!/usr/bin/env python3
import os
import time
import argparse
import requests
from modules import Colors
from modules import response 
from modules import webdriver
from modules import Reporter

ASSCI = Colors.Colors.PINK+""" 
   _____ _______    _______ _    _  _____        _____ ____  _____  ______ 
  / ____|__   __|/\|__   __| |  | |/ ____|      / ____/ __ \|  __ \|  ____|
 | (___    | |  /  \  | |  | |  | | (___ ______| |   | |  | | |  | | |__   
  \___ \   | | / /\ \ | |  | |  | |\___ \______| |   | |  | | |  | |  __|  
  ____) |  | |/ ____ \| |  | |__| |____) |     | |___| |__| | |__| | |____ 
 |_____/   |_/_/    \_\_|   \____/|_____/       \_____\____/|_____/|______|
                                 Discover The Status Of Your Web Properties                                        

"""




def Flags():
    parser = argparse.ArgumentParser(description="Get Status Code and redirects from a file of websites")
    parser.add_argument("-f", "--file", dest="UrlFile", help="File Containing Urls", metavar="URL FILE", type=str)
    parser.add_argument("-o", "--output", dest="OutFile", help="File To Contain Results", metavar="Log File", type=str)
    parser.add_argument("-H", "--https", dest="httpvar", help="https or http, Defaults to http", default='https://', type=str)
    parser.add_argument("-w", "--way-back", dest="wayback", help="Find Urls From Wayback Machine", type=str)
    parser.add_argument("-d", "--domain", dest="domain", help="Enter Top level Domain like: <domain.com> ")
    parser.add_argument("-s", "--Use_subbrute",dest='Use_Subbrute', help ="Enter -s y to use the subbrute module")
   
    args = parser.parse_args()
   
    if args.UrlFile != None and args.OutFile != None:
       print(ASSCI)
       main(args)
    elif args.wayback != None and args.OutFile != None:
         print(ASSCI)
         wayb(args)
    elif args.domain != None and args.OutFile != None:
         #print(ASSCI)
         Sublist3r(args)
    else:
        usage()




def END(args):
    # This is Too Finalise the report
    end = Reporter.Report(args.OutFile,'','','','')
    end.End_Report()




def create_report(args):
    # Create a report file 
    create = Reporter.Report(args.OutFile,'','','','')
    create.Create_Report()




def main(args):
    create_report(args)
    #Get URLS from file
    f = open(args.UrlFile, "r")
    urls = f.readlines()
    f.close()

    # Get A URL and see if it has a protocol if not add a default or user specified one!
    for url in urls:
        # Making sure there is a protocol at the start of urls  
        if 'http://' not in url and 'https://' not in url:
           protocol = args.httpvar
           if protocol != None:
                if protocol == 'http': 
                   protocol = protocol.replace("http", "http://") 
                   url = protocol+url
                elif protocol == 'https':
                   protocol = protocol.replace("https", "https://") 
                   url = protocol+url
                elif protocol == 'https://':
                     url = protocol+url
           

        try:
            # Check the Response code and then take a Screenshot the chain reaction starts here
            print("\n"+Colors.Colors.BLUE,"[*] Testing: "+url)
            sendit = response.Response(url,args.OutFile)
            sendit.SendRequest()
            #time.sleep(2)
            
        except Exception as e: 
               print(e)
               exit()
    END(args)




def wayb(args):
    print(Colors.Colors.PINK,"Going way way back, Lets Go On This Ride! - PS it might take some time!")
    create_report(args)
    url = args.wayback
    wayurl = "http://web.archive.org/web/timemap/json?url="+url+"&fl=timestamp:4,original&matchType=prefix&filter=statuscode:200&collapse=urlkey&collapse=timestamp:4"  
  
    req = requests.get(wayurl)
    data = req.json()

    if req.status_code == 200:
       for x in data:
           try:
              url1 = x[1]
              str(url1)
              if url1 != 'original':
                 print("\n"+Colors.Colors.BLUE,"[*] Testing: "+url1)
                 sendit = response.Response(url1, args.OutFile)
                 sendit.SendRequest()
           except Exception as e: 
                   print(e)
    END(args) 


def Sublist3r(args):
    # Use sublister or subrute to find subdomains
    # get Status code and screenshots and create report
    path = os.path.normpath(os.getcwd()+ os.sep + os.pardir)

    domain = args.domain 
    outfile = args.OutFile
    Subdomain_Results = 'subdomains.txt'

    SubDir = path+'/status-code/'+Subdomain_Results
    print(SubDir)
    
    # before executionchecks if a previous subdomain file is present if so delete it
    if os.path.exists(SubDir):
       os.remove(SubDir)

    if args.Use_Subbrute == 'y':
       Subcmd = './sublist3r.py '+ '-d '+domain +' -o ' +Subdomain_Results +' -v -b'

    else:
       Subcmd = './sublist3r.py '+ '-d '+domain +' -o ' +Subdomain_Results +' -v'
   
    os.system(Subcmd)

    if os.path.exists(SubDir):
       create_report(args)
       #Get URLS from file
       f = open(SubDir, "r")
       urls = f.readlines()
       f.close()

       # Get A URL and see if it has a protocol if not add a default or user specified one!
       for url in urls:
           # Making sure there is a protocol at the start of urls  
           if 'http://' not in url and 'https://' not in url:
              protocol = args.httpvar
              if protocol != None:
                 if protocol == 'http': 
                    protocol = protocol.replace("http", "http://") 
                    url = protocol+url
                 elif protocol == 'https':
                    protocol = protocol.replace("https", "https://") 
                    url = protocol+url
                 elif protocol == 'https://':
                      url = protocol+url
           

           try:
            # Check the Response code and then take a Screenshot the chain reaction starts here
            print("\n"+Colors.Colors.BLUE,"[*] Testing: "+url)
            sendit = response.Response(url,args.OutFile)
            sendit.SendRequest()
            #time.sleep(2)
            
           except Exception as e: 
                 print(Colors.Colors.RED,e)
                 exit()

    if os.path.exists(SubDir):
       os.remove(SubDir)
    END(args)


def usage():
    print()




if __name__ == '__main__':
   Flags()

#!/usr/bin/env python3 
import requests
from modules import Colors
from modules import webdriver
from modules import Reporter


class Response: 




      def __init__(self, url, OutFile):
          self.url = url.strip()
          self.outfile = OutFile.strip() 
          self.code = 0
          

      
          
           
      
      def SendRequest(self):
          try:
             req = requests.get(self.url, allow_redirects=False, timeout=4)
             self.code = req.status_code
          except Exception as e: 
               print(Colors.Colors.RED,"  [*] Could Not Connect To "+self.url+" Potentially Timed Out")
 
          if 200 <= self.code < 300: # Success
               print(Colors.Colors.GREEN,"  [*] "+self.url+" "+str(self.code)+" OK")
               
               # Write info to report
               write = Reporter.Report(self.outfile,self.url,str(self.code),'','')
               write.Write_Status()

               # Screenshot / add image tag to report
               screen = webdriver.ScreenShot(self.url, self.outfile)
               screen.Take_ScreenShot()
              
          elif 300 <= self.code < 400: # Redirections 
                 print(Colors.Colors.PINK,"  [*] "+self.url+" Redirects To: "+req.headers['location']+" Status Code: "+str(self.code))
                 self.redir = req.headers['location']

                 # Write info to report
                 write = Reporter.Report(self.outfile,self.url,str(self.code),'',self.redir)
                 write.Redir()

                 # Screenshot / add images to report
                 screen = webdriver.ScreenShot(self.url,self.outfile)
                 screen.Take_ScreenShot()

          elif 400 <= self.code < 500: # Client Errors 
                 print(Colors.Colors.RED,"  [*] "+self.url+" Client Error: Status Code: "+str(self.code))

                 #write to report
                 write = Reporter.Report(self.outfile,self.url,str(self.code),'','')
                 write.Write_Status()
                
                 # No Screenshot to be taken 

          elif 500 <= self.code < 600: # Errors
                 print(Colors.Colors.RED,"  [*] "+self.url+" Server Error: Status Code: "+str(self.code))

                 #write to report
                 write = Reporter.Report(self.outfile,self.url,str(self.code),'','')
                 write.Write_Status()
                 
                 # No Screenshot to be taken
         



if __name__ == '__main__':
  print('Module Test***************************************************************\n')
  url1 = Response('https://www.youtube.com')
  url1.SendRequest()
  print('\nModule Test End************************************************************')
  
   
          
          

#!/usr/bin/env python3
import os
import time
import datetime
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from modules import Colors
from modules import Reporter

a = os.path.normpath(os.getcwd()+ os.sep + os.pardir) # Gets Current working directory :)
#print(a+'/screenshots/')

## This Module Contains Screenshotting capabilities and other webdriven stuff!!

class ScreenShot():




      #<<-- Takes in and Sets up vars
      def __init__(self, Ss, outfile): 
          self.Ss = Ss
          self.outfile = outfile.strip()
          self.Filename = ''




      # Creates a unique filename for each screenshot.             
      def Create_Filename(self): 
          try:
             Base = 'Status-Code' #<<-- If using in another app change this to reflect your app.
             suffix = datetime.datetime.now().strftime("%y%m%d_%H%M%S")
             self.Filename = "_".join([Base, suffix+'.png'])
          except Exception:
                 print('Failed To Create A Name')



          
      def Take_ScreenShot(self): 
          # Calls Create_Filename and adds the name to var self.Filename
          ScreenShot.Create_Filename(self)
          filename = self.Filename

          writeIm = Reporter.Report(self.outfile,'','',filename,'')
          writeIm.Write_Image()
          
          try:
             # takes in web URL and sets up screenshot capabilities 
             app = self.Ss
             options = Options()
             options.add_argument("--headless")
             driver = webdriver.Firefox(options=options, executable_path=r''+a+'/status-code/modules/webdrivers/geckodriver')
             #driver = webdriver.Firefox(options=options, executable_path=r'/root/status-code/modules/webdrivers/geckodriver')
             
             # Takes the Screenshot 
             print(Colors.Colors.YELLOW,'  [*] Attempting to Screenshot '+app)
             driver.set_window_position(0, 0)
             driver.set_window_size(1024, 768)
             driver.get(app)
             time.sleep(2)# To make sure browser and webapp is loaded nice :)
             driver.save_screenshot(a+'/status-code/screenshots/'+filename)# saves screenshot
             driver.close()             
          except Exception: 
                 print(Colors.Colors.RED,'    [*] Screenshot failed')




if __name__ == '__main__':
  print('Module Test***************************************************************\n')
  screen = ScreenShot('https://www.kali.org') # <-- class tests
  screen.Take_ScreenShot()
  print('\nModule Test End************************************************************')
  #<-- end of class test

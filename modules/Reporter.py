#!/usr/bin/env python3 
import os 

a = os.path.normpath(os.getcwd()+ os.sep + os.pardir) # Gets Current working directory :)
b = '/status-code/Reports/'
c = '/status-code/'
path = a+b

path2 = a+c

#if not os.path.isdir(path):
   #print("Create the directory here")


class Report: 




      def __init__(self, filename, url, status, image, redir):
          self.filename = filename
          self.url = url
          self.status = status
          self.image = image
          self.redir = redir
          



      def End_Report(self): # write at the very end of the report when completed run
          ending = '</body></html>'
          f = open(path+self.filename+'.html', "a")
          f.write(ending)
          f.close()
          print()




      def Redir(self): # Add if any sites redirect
          table1 = '<table><tr><th>Asset Redirects: Status-code: '+self.status+'</th></tr><tr><td>URL: <a href="'+self.url+'">'+self.url+'</a></td>'     
          table2 = '<tr><td> Redirects To URL: <a href="'+self.redir+'">'+self.redir+'</a></td>'
          table = table1 + table2
          f = open(path+self.filename+'.html',"a")
          f.write(table)
          f.close()




      def Write_Image(self):# Write Third
          image = '<img src ="'+path2+'/screenshots/'+self.image+'" width="1014" height="671"><br><br>'
          table2 = '</tr><tr><td>Screenshot:'+image+'</td></tr></table><br>'
          f = open(path+self.filename+'.html', "a")
          f.write(table2)
          f.close()




      def Write_Status(self): # Write Second
          table1 = '<table><tr><th>Asset Status-code: '+self.status+'</th></tr><tr><td>URL: <a href="'+self.url+'">'+self.url+'</a></td>'
          f = open(path+self.filename+'.html',"a")
          f.write(table1)
          f.close()




      def Create_Report(self): # Start of report file Write first
          head = """<!DOCTYPE html><html><head><style>table{width:80%;margin-left: auto;margin-right: auto;border: 1px solid black;}th{width:100%;font-size: 20px;border-bottom: 1px solid black}
                  img{display: block;margin-left: auto;margin- right: auto;border:1px solid #021a40;}
                  div{text-align: left;vertical-align: middle;}h1{text-align: center;}</style></head><body><h1>STATUS-CODE Version 0.1</h1><div><h1>Results:</h1>
           """
          
          #This will add html and all that jazz to the create the report only needs done once
          f = open(path+self.filename+'.html',"a")
          f.write(head)
          f.close()




if __name__ == '__main__':
   print('Module Test***************************************************************\n')
   test = Report('test3.html','https://www.youtube.com','200','','','')
   test.Create_Report()
   test.Report_Items()
   test.Report_Items()
   test.Report_Items()
   test2 = Report('test3.html','https://www.youtube.com','301','image1','https://youtube.com/1/','image2')
   test2.Report_Items_Redir()
   print('\nModule Test End************************************************************')
